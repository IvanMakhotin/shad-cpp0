cmake_minimum_required(VERSION 2.8)
project(abs)

include(../common.cmake)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=undefined -fno-sanitize-recover")

add_executable(abs abs.cpp)
